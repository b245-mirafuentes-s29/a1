/*db.users.insertMany([
            {
                firstName: "Jane",
                lastName: "Doe",
                age: 21,
                contact: {
                    phone: "87654321",
                    email: "janedoe@gmail.com"
                },
                courses: [ "CSS", "Javascript", "Python" ],
                department: "HR"
            },
            {
                firstName: "Stephen",
                lastName: "Hawking",
                age: 76,
                contact: {
                    phone: "87654321",
                    email: "stephenhawking@gmail.com"
                },
                courses: [ "Python", "React", "PHP" ],
                department: "HR"
            },
            {
                firstName: "Neil",
                lastName: "Armstrong",
                age: 82,
                contact: {
                    phone: "87654321",
                    email: "neilarmstrong@gmail.com"
                },
                courses: [ "React", "Laravel", "Sass" ],
                department: "HR"
            }
        ]);
db.users.insertOne({
     firstName: "Bill",
            lastName: "Gates",
            age: 65,
            contact: {
                phone: "12345678",
                email: "bill@gmail.com"
            },
            courses:  ["PHP", "Laravel", "HTML"],
            department: "Operations",
        });*/

db.users.find(
{
 $or: [
    {firstName: {$regex: "s", $options: "i"}},
    {lastName: {$regex: "d", $options: "i"}}
 ]
},
{
    firstName: 1,
    lastName: 1,
    _id: 0
}
)

db.users.find(
{
 $and: [
    {department: "HR"},
    {age: {$gte: 70}}
 ]
}
)

db.users.find(
{
 $and: [
    {firstName: {$regex: "e"}},
    {age: {$lte: 30}}
 ]
}
)